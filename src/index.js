import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


const monRoot = ReactDOM.createRoot(document.getElementById('App'));
monRoot.render(
    <App/>
)
