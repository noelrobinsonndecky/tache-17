import React, { useState } from 'react';
import './Styles/App.css';


// Lecture des éléments à faire

// Mon composant Todo
const Todo = ({todo, index, completeTodo, removeTodo}) => {

  return (
    <div className='todo'style={{ backgroundColor: todo.isCompleted ? "green" : "orange"}}>
      
      {todo.text}

      <div>
          <button className='completed' onClick={() => completeTodo(index)}>Complete</button>
          <button className='supp' onClick={() => removeTodo(index)}>x</button>
      </div>

    </div>
  );
};


// Création des éléments à faire

function TodoForm({ addTodo }) {
  const [value, setValue] = React.useState("");

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        className="input"
        placeholder='Vos Taches à faire'
        value={value}
        onChange={e => setValue(e.target.value)}
      />
    </form>
  );
}

// Mon composant App
const App = () => {
  const [todos, setTodos] = useState ([
    { 
      text: "Apprendre Reactjs",
      isCompleted: false
    },
    { 
      text: "Rencontrer un amis",
      isCompleted: false 
    },
    { 
      text: "Faire un très bon TodoList",
      isCompleted: false
     }
  ])
  
  // Ajout d'éléments dans notre liste
  const addTodo = text => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  // Mise à jour des éléments
  const completeTodo = index => {
    const newTodos = [...todos];
    newTodos[index].isCompleted = true;
    setTodos(newTodos);
  };

  // Supression des éléments
  const removeTodo = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };

  return (
    <div className="app">
      <div className="todo-list">
        <h1>Ma ToDoList</h1>
        {todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            // Mise à jour des éléments
            completeTodo={completeTodo}
            // Supression d'éléments
            removeTodo={removeTodo}
          />
        ))}
          <TodoForm addTodo={addTodo} />
      </div>
    </div>
  );
}

export default App;
