// Comment créer une application React To-Do avec React Hooks?

/**
 * Introduction:
 * 
 * React est une bibliothèque JavaScript frontale qui peut être utilisée 
 * pour créer des interfaces utilisateur interactives pour votre application.
 * 
 * Pour cette partie, nous allons créer une application de tache.
 * Notre application devra afficher les tâches, ajouter de nouvelles tâches, 
 * marquer des tâches comme terminées et supprimer des tâches. 
 * Ces actions toucheront les quatre aspects d'une application CRUD (Créer, Lire, Mettre à jour et Supprimer).
 * 
 * Cette application intégrera à la place React Hooks . 
 * React Hooks permet aux composants fonctionnels d'avoir un état et d'utiliser des méthodes de cycle de vie, 
 * ce qui nous permet d'éviter d'utiliser des composants de classe et d'avoir un code plus modulaire et lisible.
 * 
 * Les composants fonctionnels:
 * Dans les versions précédentes de React, les composants fonctionnels étaient incapables de gérer l'état, 
 * mais maintenant, en utilisant Hooks, ils le peuvent.
 * 
 * Le premier paramètre, [todos], est ce que vous allez nommer votre état.
 * Le deuxième paramètre, [setTodos], est ce que vous allez utiliser pour définir l'état.
 * 
 * Le crochet de useState() est ce que React utilise pour se connecter à l'état ou au cycle de vie du composant.
 * 
 * En utilisant la méthode JavaScriptmap(), 
 * vous pourrez créer un nouveau tableau d'éléments en mappant sur les éléments de l'état et en les affichant par index.
 * 
 * Remarquez qu'il n'y a pas de this.state. Avec les nouveaux React Hooks, 
 * vous n'en aurez plus l'utilité this.state
 * puisque les nouveaux Hooks comprennent qu'il va être sous-entendu à certains endroits.
 * 
 * 
 * 
*/